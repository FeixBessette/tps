import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;


public class GitExtract {

    /**
     * Clone les fichiers d'un dépôt Git dans un répertoire temporaire
     *
     * @param url  adresse d'une page web d'un dépôt Git
     */
    public static void cloneRepository(String url, File directory) throws IOException, InterruptedException {
        if (directory.exists()) {
            deleteDirectory(directory);
        }
        System.out.println("Cloning...");
        ProcessBuilder processBuilder = new ProcessBuilder("git","clone",url, directory.getName());
        processBuilder.directory(new File(new File(directory.getAbsolutePath()).getParent()));
        Process cloning = processBuilder.start();
        cloning.waitFor();
        System.out.println("Done.");
    }

    /**
     * Extrait tous les identifiants des versions d'un répertoire git
     *
     * @param directory  répertoire git
     * @return  ArrayList<String> liste de tous les identifiants
     */
    public static ArrayList<String> getRepositoryIds(File directory) throws IOException {
        ArrayList<String> commits = new ArrayList<>();
        ProcessBuilder processBuilder = new ProcessBuilder("git","rev-list","head");
        processBuilder.directory(directory);
        Process listIds = processBuilder.start();

        java.io.InputStream is = listIds.getInputStream();
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        while (s.hasNextLine()) {
            commits.add(s.nextLine());
        }
        return commits;
    }

    /**
     * Reset un répertoire Git local à une version spécifique
     *
     * @param id  identifiant d'une version du dépot Git
     * @param directory  répertoire local du dépot Git
     */
    public static void setRepositoryVersion(String id, File directory) throws IOException, InterruptedException {
        ProcessBuilder resetProcess = new ProcessBuilder("git","reset","--hard",id);
        resetProcess.directory(directory);
        Process reset = resetProcess.start();
        reset.waitFor();

        ProcessBuilder cleanProcess = new ProcessBuilder("git","clean","-fd");
        cleanProcess.directory(directory);
        Process clean = cleanProcess.start();
        clean.waitFor();
    }

    /**
     * Supprime un fichier ou un répertoire
     *
     * @param file Répertoire ou fichier à supprimer
     */
    public static void deleteDirectory(File file) {
        if (!file.exists()) return;
        if (file.isDirectory()) {
            for (File f : file.listFiles()) {
                deleteDirectory(f);
            }
        }
        file.delete();
    }

    /**
     * Appel du programme pour calculer les métriques sur la version actuelle
     *
     * @param temp  répertoire sur lequel appliquer la collecte de métriques
     */
    public static void getJavaMetrics(File temp) throws IOException, InterruptedException {
        ProcessBuilder processBuilder = new ProcessBuilder("java","-jar","JavaMetrics.jar",
                temp.getAbsolutePath());
        processBuilder.directory(new File(new File(temp.getAbsolutePath()).getParent()));
        Process processjava = processBuilder.start();
        processjava.waitFor();
    }

    public static void main(String[] args) throws Exception {
        System.out.print("Git url: ");
        String url = new Scanner(System.in).nextLine();
        // dossier temporaire pour traiter les fichiers
        File temp = new File("temp");
        // cloner le répertoire dans le dossier temporaire
        cloneRepository(url, temp);
        // fichier contenant les métriques cibles
        FileWriter csvWriterInit = new FileWriter(new File("RepoVersionsMetrics.csv"));
        FileWriter csvAppend = new FileWriter(new File("RepoVersionsMetrics.csv"), true);
        csvWriterInit.write("id_version, n_classes, m_c_BC\n");
        csvWriterInit.flush();
        csvWriterInit.close();
        // extractions de tous les identifiants du répertoire
        ArrayList<String> commits = getRepositoryIds(temp);
        // taille de l'échantillon à extraire
        int sampleSize = (int) Math.ceil(commits.size() * 0.1);
        // message
        System.out.println("Writing results in RepoVersionsMetrics.csv");
        System.out.println("Please wait...");
        // extraction des métriques de l'échantillon
        for (int i = 0; i < sampleSize; i++) {
            // sélection d'un identifiant aléatoire
            int randomId = (int) (Math.random() * commits.size());
            // reset du dépot local à la version obtenue
            setRepositoryVersion(commits.get(randomId), temp);
            // écrire la version dans le csv
            csvAppend.write(commits.get(randomId) + ", ");
            csvAppend.flush();
            // appel au programme pour extraire les métriques des fichiers .java
            getJavaMetrics(temp);
        }
        System.out.println("Done.");
        csvAppend.close();
        // suppression du fichier git temporaire
        deleteDirectory(temp);
    }

}
