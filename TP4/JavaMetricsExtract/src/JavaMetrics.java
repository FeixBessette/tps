import java.io.*;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;

public class JavaMetrics {

    public int classLOC = 0;
    public int classCLOC = 0;
    public double classBC = 0;
    public int wmc = 1;
    public int operationCount = 0;
    public boolean classIsOpen = false;
    public boolean methodIsOpen = false;
    public boolean beforeMethode = false;
    public boolean multilineComment = false;

    public void analyzeClassLine(String line) {
        String modLine = line.trim().replaceAll("\\s{2,}", " ");
        String[] lineWords = modLine.split("\\s");

        if (lineWords.length > 0) {
            ++classLOC;
        }

        if ((line.contains("class") || line.contains("interface") || line.contains("enum")) &&
                !line.contains("//") && !line.contains("/*") && !line.contains("*"))  {
            classIsOpen = true;
        }

        if (line.contains("}") && classIsOpen && !methodIsOpen && line.length() == 1) {
            classIsOpen = false;
            double classDC = (double) classCLOC / (double) classLOC;
            classBC = classDC / (double) wmc;
        }
    }

    public void analyzeMethodsLine(String line) {
        String modLine = line.trim().replaceAll("\\s{2,}", " ");
        String[] lineWords = modLine.split("\\s");

        if (methodIsOpen && line.contains("{")) {
            operationCount++;
        }

        if (methodIsOpen && line.contains("case") || ((line.contains("if") || line.contains("while") || line.contains("else") ||
                line.contains("do") || line.contains("for")) && line.contains("{"))) {
            wmc++;
        }

        if (line.contains("{")  && classIsOpen && !methodIsOpen && !line.contains("class") && line.contains("\t")) {
            methodIsOpen = true;
        }

        if ( line.contains("}") && methodIsOpen) {
            if (operationCount == 0) {
                methodIsOpen = false;
            }
            else if (operationCount > 1) {
                operationCount--;
            }
        }

    }

    public void analyzeCommentsLine(String line) {
        if (line.contains("/*")) {
            // avant methode
            if (!methodIsOpen && classIsOpen) {
                beforeMethode = true;
            }
            // dans une méthode
            else if (methodIsOpen)
                multilineComment = true;
        }

        // lignes de commentaire simple ou commentaire imbriqué
        if (line.contains("//")) {
            ++classCLOC;
        }

        // ligne de commentaire liée à la spécification de la méthode
        if (beforeMethode) {
            ++classCLOC;
        }

        // commentaire multiligne
        if (multilineComment) {
            ++classCLOC;
        }

        // fermeture du commentaire multiligne ou javadoc
        if (line.contains("*/")) {
            multilineComment = false;
            beforeMethode = false;
        }
    }

    public static double getMedian(ArrayList<Double> arrayList) {
        double mediane = 0;
        if (arrayList.size() == 0) return mediane;
        if (arrayList.size() % 2 == 0) {
            int index1 = arrayList.size() / 2 - 1;
            int index2 = arrayList.size() / 2;
            mediane = arrayList.get(index1) + arrayList.get(index2) / 2;
        } else {
            int milieu = (arrayList.size() + 1 ) / 2 - 1;
            mediane = arrayList.get(milieu);
        }
        return mediane;
    }

    public static void createCSV(String[] metrics) {
        NumberFormat formatter = new DecimalFormat("#0.000000");
        try {
            FileWriter csvWriter = new FileWriter(new File("RepoVersionsMetrics.csv"), true);
            csvWriter.write(metrics[0] + ", " + formatter.format(Double.valueOf(metrics[1])) +"\n");
            csvWriter.flush();
            csvWriter.close();
        } catch (Exception e) {
            System.err.println("Error creating CSV files");
        }

    }

    public static void main(String[] args) throws IOException {
        // target directory
        File directory = new File(args[0]);
        // Classes BC of java files
        ArrayList<Double> classesBC = new ArrayList<>();
        CSV csv = new CSV();
        LinkedList<File> javaFiles = csv.searchFiles(directory);
        for (File file : javaFiles) {
            JavaMetrics javaFile = new JavaMetrics();
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;
            while ((line = br.readLine()) != null) {
                if (line.length() == 0) continue;
                javaFile.analyzeClassLine(line);
                javaFile.analyzeMethodsLine(line);
                javaFile.analyzeCommentsLine(line);
            }
            classesBC.add(javaFile.classBC);
        }
        Collections.sort(classesBC);
        double repoMediane = getMedian(classesBC);
        double classesCount = javaFiles.size();
        String[] metrics = {String.valueOf((int)classesCount), String.valueOf(repoMediane)};
        createCSV(metrics);
    }

}
