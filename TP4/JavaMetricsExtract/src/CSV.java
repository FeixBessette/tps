import java.io.File;
import java.io.FileWriter;
import java.text.DecimalFormat;
import java.util.*;

/**
 * Fait la recherche sur les fichiers désirés et la création des fichers .csv
 */
public class CSV {
    LinkedList<File> matchedFiles = new LinkedList<File>();
    private final DecimalFormat roundedFormat = new DecimalFormat("#.######");

    /**
     * Recherche récursivement dans les dossiers les fichiers qui correspondent
     * à l'extansion voulue.
     * @param directory    dossier racine
     * @return             liste des fichiers trouvés
     */
    public LinkedList<File> searchFiles(File directory) {
        for (File file : directory.listFiles()) {
            // chercher le dossier et sous-dossiers
            if (file.isDirectory())
                searchFiles(file);
                // créer un fichier CSV pour les fichiers .java
            else if (file.isFile() && file.getName().endsWith(".java"))
                matchedFiles.add(file);
        }
        return matchedFiles;
    }

    /**
     * Pour le fichier .java, crée un fichier .csv pour les données des classes
     * et un fichier .csv pour les données des méthodes
     * @param file          fichier à traiter
     */
    public void createCSV(File file, String[] metrics) {
        try {
            FileWriter csvWriter = new FileWriter(file, true);
            csvWriter.write(metrics[0] + ", " + metrics[1] + ", " +
                    roundedFormat.format(Double.parseDouble(metrics[2])) +"\n");
            csvWriter.flush();
            csvWriter.close();
        } catch (Exception e) {
            System.err.println("Error creating CSV files");
        }
    }
}

