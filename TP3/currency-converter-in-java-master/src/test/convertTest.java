package test;
import currencyConverter.Currency;
import currencyConverter.MainWindow;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class convertTest {
    @Test
    public void test() {
        // Tests boite noire pour : Main currencyConverter.MainWindow.convert(String, String, ArrayList, Double).
        ArrayList<Currency> currencies = Currency.init();
        // Approche de partition du domaine des entrées en classes d’équivalence
        //D1 (assertEquals)
        assertNotEquals(1.28, MainWindow.convert("US Dollar", "CA Dollar", currencies, 1.0));
        //D2
        assertEquals(0d, MainWindow.convert("Hungarian forint", "US Dollar", currencies, 1.00));
        //D3
        assertEquals(0d, MainWindow.convert("US Dollar", "Hungarian forint", currencies, 1.00));
        //D4
        assertEquals(0d, MainWindow.convert("Hungarian forint", "Mexican Peso", currencies, 1.00));
        //D5
        assertEquals(0d, MainWindow.convert("Hungarian forint", "CA Dollar", currencies, -1.00));
        //D6
        assertEquals(0d, MainWindow.convert("US Dollar", "Hungarian forint", currencies, -1.00));
        //D7
        assertEquals(-0.93, MainWindow.convert("US Dollar", "Euro", currencies, -1.00));
        //D8
        assertNotEquals(0.067, MainWindow.convert("Hungarian forint", "Mexican Peso", currencies, -1.00));
        //D9
        assertNotEquals(Double.MAX_VALUE + 0.01, MainWindow.convert("Swiss Franc", "Swiss Franc", currencies, Double.MAX_VALUE + 0.01));

        // Approche d’analyse des valeurs frontières
        assertEquals(-929.07, MainWindow.convert("US Dollar", "Euro", currencies, -999.0));
        assertEquals(-0.01, MainWindow.convert("US Dollar", "Euro", currencies, -0.01));
        assertEquals(0.0, MainWindow.convert("US Dollar", "Euro", currencies, 0.0));
        assertEquals(929.07, MainWindow.convert("US Dollar", "Euro", currencies, 999.0));
        assertNotEquals(Double.MAX_VALUE * 0.93, MainWindow.convert("US Dollar", "Euro", currencies, Double.MAX_VALUE));
        assertNotEquals((Double.MAX_VALUE + 0.01) * 0.93, MainWindow.convert("US Dollar", "Euro", currencies, Double.MAX_VALUE + 0.01));


        // Tests boite noire pour : currencyConverter.Currency.convert(Double, Double)
        // D1
        assertEquals(2.0, Currency.convert(2.00, 1.00));
        // D2
        assertEquals(-1.0, Currency.convert(-1.00, 1.00));
        // D3
        assertEquals(-1.0, Currency.convert(1.00, -1.00));
        // D4
        assertEquals(-1.0, Currency.convert(-1.00, 1.00));
        // D5
        assertNotEquals(Double.MAX_VALUE * 0.67, Currency.convert(Double.MAX_VALUE, 0.67));
        // D6
        assertNotEquals(10d * Double.MAX_VALUE, Currency.convert(10d, Double.MAX_VALUE));

        // Approche d’analyse des valeurs frontières
        // D1 : entrée valide pour les valeurs frontières de amount
        assertEquals(0.0, Currency.convert(0.0, 1.0));
        assertEquals(999.0, Currency.convert(999.0, 1.0));
        // D2 : entrée invalide pour les valeurs frontières de amount
        assertEquals(-0.01, Currency.convert(-0.01, 1.0));
        assertEquals(-999.0, Currency.convert(-999.0, 1.0));
        // D3 : : entrée valide pour les valeurs frontières de exchangeValue
        assertEquals(0.0, Currency.convert(1.0, 0.0));
        assertEquals(999.0, Currency.convert(1.0, 999.0));
        // D4 : entrée invalide pour les valeurs frontières de exchangeValue
        assertEquals(-0.01, Currency.convert(1.0, -0.01));
        assertEquals(-999.0, Currency.convert(1.0, -999.0));
        // D5 : limite supérieure d'une entrée valide
        assertNotEquals(Double.MAX_VALUE, Currency.convert(Double.MAX_VALUE, Double.MAX_VALUE));
        // D6 : dépassement de la limite supérieure
        assertNotEquals(Double.MAX_VALUE+0.01, Currency.convert(Double.MAX_VALUE+0.01, Double.MAX_VALUE+0.01));


        // TESTS BOITE BLANCHE pour : Main currencyConverter.MainWindow.convert(String, String, ArrayList, Double)
        // A. Critère de couverture des instruction
        assertEquals(1.86, MainWindow.convert("US Dollar", "Euro", currencies, 2.0));

        // B. Critère de couverture des arcs du graphe de flot de contrôle
        // D1
        assertEquals(2.16, MainWindow.convert("Euro", "Swiss Franc", currencies, 2.0));
        // D2
        assertEquals(0.0, MainWindow.convert("Euro", "Hungarian Forint", currencies, 1.0));
        // D3
        assertEquals(0.0, MainWindow.convert("Hungarian Forint", "Euro", currencies, 1.0));

        // C. Critère de couverture des chemins indépendants
        //D1 : (1,2,3,4,8,14)
        ArrayList<Currency> emptyCurrencies = new ArrayList<>();
        assertEquals(0.0, MainWindow.convert("US Dollar", "Euro", emptyCurrencies, 1.0));
        //D2 : (1,2,3,4,5,6,7,8,9,10,11,12,13,14)
        assertEquals(1d, MainWindow.convert("US Dollar", "US Dollar", currencies, 1.0));
        //D3 : (1,2,3,4,5,6,7,8,9,10,9,14)
        assertEquals(0.0, MainWindow.convert("Hungarian Forint", "US Dollar", currencies, 1.0));
        //D4 : (1,2,3,4,5,4,8,14)
        assertEquals(0.0, MainWindow.convert("Hungarian Forint", "Mexican Pesos", currencies, 1.0));
        //D5 : (1,2,3,4,5,6,7,8,14)
        currencies.add( new Currency("JPN", null) );
        assertEquals(0.0, MainWindow.convert("US Dollar", "JPN", currencies, 1.0));
        currencies.remove(6);

        // E. Couverture des i-chemins
        //D1 : saute la première boucle
        assertEquals(0.0, MainWindow.convert("US Dollar", "Euro", emptyCurrencies, 1.0));
        //D2 : une itération pour chaque boucle
        assertEquals(1.0, MainWindow.convert("US Dollar", "US Dollar", currencies, 1.0));
        //D3 : deux itérations pour chaque boucle
        assertEquals(1.0, MainWindow.convert("Euro", "Euro", currencies, 1.0));
        //D4 :  n-1 itérations  pour chaque boucle
        assertEquals(1.0, MainWindow.convert("Chinese Yuan Renminbi", "Chinese Yuan Renminbi", currencies, 1.0));
        //D5 : n itérations  pour chaque boucle
        assertEquals(1.0, MainWindow.convert("Japanese Yen", "Japanese Yen", currencies, 1.0));
        //D6 :  m itérations (m<n) pour la première boucle  et  n+1 itérations pour la deuxième
        assertEquals(0.0, MainWindow.convert("Hungarian Forint", "Swiss Franc", currencies, 1.0));
        //D7 :  m itérations (m<n) pour la deuxième boucle et  n+1 itérations pour la première
        assertEquals(0.0, MainWindow.convert("Swiss Franc", "Hungarian Forint", currencies, 1.0));

        //TESTS BOITE BLANCHE pour : currencyConverter.Currency.convert(Double, Double)
        // A. Critère de couverture des instructions
        assertEquals(2.0, Currency.convert(2.0, 1.0));


    }
}
