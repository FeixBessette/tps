import java.io.File;
import java.io.FileWriter;
import java.text.DecimalFormat;
import java.util.*;

/**
 * Fait la recherche sur les fichiers désirés et la création des fichers .csv
 */
public class CSV {
    static LinkedList<File> matchedFiles = new LinkedList<>();
    private static final DecimalFormat roundedFormat = new DecimalFormat("0.0000");

    /**
     * Recherche récursivement dans les dossiers les fichiers qui correspondent
     * à l'extansion voulue.
     * @param directory    dossier racine
     * @return             liste des fichiers trouvés
     */
    public static LinkedList<File> searchFiles(File directory) {
        for (File file : directory.listFiles()) {
            // chercher le dossier et sous-dossiers
            if (file.isDirectory())
                searchFiles(file);
            // créer un fichier CSV pour les fichiers .java
            else if (file.isFile() && file.getName().endsWith(".java"))
                matchedFiles.add(file);
        }
        return matchedFiles;
    }

    /**
     * Pour le fichier .java, crée un fichier .csv pour les données des classes
     * et un fichier .csv pour les données des méthodes
     * @param file          fichier à traiter
     * @param classesList   données sur les classes du fichier
     * @param methodList    données sur les méthodes du fichier
     */
    public static void createCSV(File file, LinkedList<ClassData> classesList, LinkedList<MethodData> methodList) {
        try {
            FileWriter  csvWriter = new FileWriter (file.getParent() + "\\"
                    + file.getName().replaceAll(".java", "") +"_methods.csv");

            csvWriter.write("chemin, classe, methode, methode_LOC, methode_CLOC, methode_DC, methode_CC, " +
                    "methode_BC\n");

            for (MethodData methodData : methodList) {
                csvWriter.write(file.getAbsolutePath() + ", " + methodData.getClassName() + ", " + methodData.getMethodName() +
                        ", " + methodData.getLOC() + ", " + methodData.getCLOC() + ", " + roundedFormat.format(methodData.getDC()) + ", " +
                        methodData.getCC() + ", " + roundedFormat.format(methodData.getBC()) + "\n");
            }
            csvWriter.flush();

            csvWriter = new FileWriter(file.getParent() + "\\"
                    + file.getName().replaceAll(".java", "") +"_classes.csv");

            csvWriter.write("chemin, classe, classe_LOC, classe_CLOC, classe_DC, classe_BC, classe_WMC\n");
            for (ClassData classData : classesList) {
                csvWriter.write(file.getAbsolutePath() + ", " + classData.getName() + ", " + classData.getLOC()
                        + ", " + classData.getCLOC() + ", " + roundedFormat.format(classData.getDC()) + ", " +
                        roundedFormat.format(classData.getBC()) + ", "+ classData.getWMC() +"\n");
            }
            csvWriter.flush();
            csvWriter.close();

        } catch (Exception e) {
            System.err.println("Error creating CSV files");
        }
    }
}

