/**
 * Modélise les métriques observées d'une classe et fournit les opérations
 * pour les extraires
 */
public class ClassData {
    private final String name;
    private final int LOC, CLOC, WMC;
    private final double DC, BC;

    /**
     * Attributs de la classe
     *
     * @param name  nom de la classe
     * @param LOC   nombre de lignes de code
     * @param CLOC  nombre de lignes commentaires
     * @param DC    densité de commentaire
     * @param BC    degré selon lequel la classe est bien commentée
     * @param WMC   somme pondérée des complexités des méthodes
     */
    public ClassData(String name, int LOC, int CLOC, double DC, double BC, int WMC) {
        this.name = name;
        this.LOC = LOC;
        this.CLOC = CLOC;
        this.DC = DC;
        this.BC = BC;
        this.WMC = WMC;
    }

    public String getName() {
        return name;
    }

    public int getLOC() {
        return LOC;
    }

    public int getCLOC() {
        return CLOC;
    }

    public double getDC() {
        return DC;
    }

    public double getBC() {
        return BC;
    }

    public int getWMC() {
        return WMC;
    }
}
