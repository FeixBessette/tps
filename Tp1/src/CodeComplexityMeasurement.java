// Auteurs: Félix Bessette & Juan David Zuniga
// TRAVAIL PRATIQUE 1

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Scanner;


/**
 * La classe permet d'extraire les informations d'un fichier Java afin de
 * mesurer sa sous-documentation
 */
public class CodeComplexityMeasurement {

    // nombre de lignes de code d’une classe
    public int classLOC = 0;

    // nombre de lignes de commentaire d'une classe
    public int classCLOC = 0;

    // weighted methods per class
    public int wmc = 0;

    // nombre de lignes de code d’une méthode
    public int methodLOC = 0;

    // nombre de lignes de commentaire d'une méthode
    public int methodCLOC = 0;

    // indique si le commentaire est rattaché à une déclaration de méthode tel qu'une spécification
    public boolean beforeMethode = false;

    // indique lorsqu'on se trouve dans une méthode
    public boolean methodIsOpen = false;

    // indique si les lignes de codes lues sont des commentaires multilignes
    public boolean multilineComment = false;

    // indique lorsqu'on se trouve dans une classe
    public boolean classIsOpen = false;

    //Compte les '{' et les '}' pour savoir si ces derniers sont liés à une opération (ex : for, if,..)
    public int operationCount = 0;

    // nombre de predicats pour chaque méthode
    public int predicatCount = 1;

    // nom de la classe
    public String className;

    // nom de la méthode
    public String methodName;

    // données sur les classes
    public final LinkedList<ClassData> classesDataList = new LinkedList<>();

    // données sur les méthodes
    public final LinkedList<MethodData> methodsDataList = new LinkedList<>();


    /**
     * Extrait le nom, comptabilise le nombre de lignes d'une classe et crée un Objet
     * contenant les informations de la classe
     * @param line  ligne courante du fichier java
     */
    public void analyzeClassLine(String line) {
        // extraction des mots dans la ligne de code
        String modLine = line.trim().replaceAll("\\s{2,}", " ");
        String[] lineWords = modLine.split("\\s");

        // toute ligne non-vide appartient à la classe
        if (lineWords.length > 0) {
            ++classLOC;
        }

        boolean isOpenBrace = lineWords[lineWords.length - 1].equals("{");
        boolean isCloseBrace = lineWords[lineWords.length - 1].equals("}");

        // extraction du nom de la classe
        if (line.contains("class")) {
            classIsOpen = true;
            for (int i = 0; i < lineWords.length; i++) {
                if (lineWords[i].equals("class"))
                    className = lineWords[++i];
            }
        }

        // collecte les données de la classe à sa fermeture
        if (isCloseBrace && classIsOpen && !methodIsOpen) {
            classIsOpen = false;
            // densité de commentaires pour une classe
            double classDC = (double) classCLOC / (double) classLOC;
            // degré selon lequel une classe est bien commentée
            double classBC = 0;
            if (wmc != 0)
                classBC = classDC / (double) wmc;
            ClassData classData = new ClassData(className, classLOC, classCLOC, classDC, classBC, wmc);
            classesDataList.add(classData);
        }
    }

    /**
     * Extrait le nom et paramètres de la méthode
     * Comptabilise le nombre de lignes de celle-ci et crée un Objet contenant ses informations
     * @param line  ligne courante du fichier java
     */
    public void analyzeMethodsLine(String line) {
        // extraction des mots dans la ligne de code
        String modLine = line.trim().replaceAll("\\s{2,}", " ");
        String[] lineWords = modLine.split("\\s");

        boolean isOpenBrace = lineWords[lineWords.length - 1].equals("{");

        // les accolades dans un méthodes sont liées à des opérations
        if (methodIsOpen && line.contains("{")) {
            ++operationCount;
        }

        // incrément du nombre de prédicats
        if (methodIsOpen && (line.contains("case") || (line.contains("if") || line.contains("while") || line.contains("else") ||
                line.contains("do") || line.contains("for")) && line.contains("{"))) {
            ++predicatCount;
        }


        // début d'une méthode
        if (isOpenBrace && !methodIsOpen && classIsOpen && !line.contains("class")){
            methodIsOpen = true;
            // si la méthode a le mots "static" ou "abstract"
            boolean rightShift =  false;
            // si la visibilité est implicitement package private
            boolean isPackagePrivate = true;
            int i = 1;
            // retrait des accolades et paranthèse dans la ligne
            String noBracketsLine = line.replaceAll("[(){]", " ");
            noBracketsLine = noBracketsLine.trim().replaceAll("\\s{2,}", " ");
            String[] words = noBracketsLine.split("\\s");

            int methodDeclarationSize = words.length;

            // déplacement à droite dans le tableau si ces mots sont présent
            if (line.contains("static") || line.contains("abstract")) {
                ++i;
                rightShift = true;
            }

            // visibilité autre que celle du package
            if (words[0].equals("public") || words[0].equals("protected") || words[0].equals("private")) {
                ++i;
                isPackagePrivate = false;
            }

            // limitation des paramètres à extraire s'il y a un throws
            if (line.contains("throws"))
                methodDeclarationSize -= 2;
            // extraction des données de la méthode si ce n'est pas un constructeur
            if (!className.equals(words[0]) && !className.equals(words[1])) {
                // extraction du nom
                if (rightShift && !isPackagePrivate)
                    methodName = words[3];
                else if (!rightShift && isPackagePrivate)
                    methodName = words[1];
                else
                    methodName = words[2];
                // extraction des arguments
                for (; i < methodDeclarationSize; i++) {
                    if (((rightShift && !isPackagePrivate) || (!rightShift && isPackagePrivate)) && i % 2 == 0 )
                        methodName += "_" + words[i];
                    else if (((rightShift && isPackagePrivate) || (!rightShift && !isPackagePrivate)) && i % 2 == 1)
                        methodName += "_" + words[i];
                }
            }
            // extraction du nom des construteurs
            else if (isPackagePrivate)
                methodName = words[0];
            else
                methodName = words[1];
            // extraction des arguments du constructeur
            for (; i < methodDeclarationSize; i++) {
                if (isPackagePrivate && i % 2 == 1)
                    methodName += "_" + words[i];
                else if (!isPackagePrivate && i % 2 == 0)
                    methodName += "_" + words[i];
            }
        }

        // temps qu'on est dans une méthode, toute ligne non-vide lui appartient
        if (methodIsOpen) {
            ++methodLOC;
        }

        // traitement sur les accolades de fermeture
        if (methodIsOpen && line.contains("}")) {

            // collecte des données de la méthode à sa fermeture
            if (operationCount == 0) {
                methodIsOpen = false;
                // densité de commentaires pour une méthode
                double methodDC =(double) methodCLOC / (double) methodLOC;
                // degré selon lequel une méthode est bien commentée
                double methodBC = methodDC / (double) predicatCount;
                // contribution de la complexité d'une méthode à la classe
                wmc += predicatCount;
                MethodData methodData = new MethodData(methodName, className, methodLOC, methodCLOC, methodDC,
                        predicatCount, methodBC);
                methodsDataList.add(methodData);
                methodName = null;
                predicatCount = 1;
                methodLOC = 0;
                methodCLOC = 0;
            }
            // l'accolade de fermeture est celle d'une opération
            else if (operationCount > 0)
                --operationCount;
        }
    }

    /**
     * Traite les lignes de commentaires et les attribues à sa classe et sa méthode
     * @param line  ligne courante du fichier java
     */
    public void analyzeCommentsLine(String line) {

        // traitement sur les commentaires multilignes ou javadoc
        if (line.contains("/*")) {
            // avant methode
            if (!methodIsOpen && classIsOpen) {
                beforeMethode = true;
            }
            // dans une méthode
            else if (methodIsOpen)
                multilineComment = true;
        }

        // lignes de commentaire simple ou commentaire imbriqué
        if (line.contains("//")) {
            ++classCLOC;
            // commentaire interne d'une méthode
            if (methodIsOpen)
                methodCLOC++;
        }

        // ligne de commentaire liée à la spécification de la méthode
        if (beforeMethode) {
            ++classCLOC;
            methodCLOC++;
            methodLOC++;
        }

        // commentaire multiligne
        if (multilineComment) {
            ++classCLOC;
            // commentaire interne d'une méthode
            if (methodIsOpen)
                methodCLOC++;
        }

        // fermeture du commentaire multiligne ou javadoc
        if (line.contains("*/")) {
            multilineComment = false;
            beforeMethode = false;
        }
    }

    public static void main(String[] args) throws Exception {
        System.out.print("Répertoire: ");
        Scanner scanner = new Scanner(System.in);
        File directoryLocation = new File(scanner.nextLine());
        // liste des fichier java à traiter
        LinkedList<File> javaFiles = CSV.searchFiles(directoryLocation);
        // traitement sur les lignes des fichiers
        for (File file : javaFiles) {
            CodeComplexityMeasurement javaFile = new CodeComplexityMeasurement();
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;
            while ((line = br.readLine()) != null) {
                if (line.isEmpty()) continue;
                javaFile.analyzeClassLine(line);
                javaFile.analyzeMethodsLine(line);
                javaFile.analyzeCommentsLine(line);
            }
            // création du CSV pour le fichier java
            CSV.createCSV(file, javaFile.classesDataList, javaFile.methodsDataList);
        }
    }

}
