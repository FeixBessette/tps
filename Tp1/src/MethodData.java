/**
 * Modélise les métriques observées d'une méthode et fournit les opérations
 * pour les extraires
 */
public class MethodData {
    private final String methodName, className;
    private final int LOC, CLOC, CC;
    private final double DC, BC;

    /**
     * Attributs de la méthode
     *
     * @param methodName    nom de la méthode
     * @param className     nom de la classe auquelle elle appartient
     * @param LOC           nombre de lignes de code
     * @param CLOC          nombre de lignes commentaires
     * @param DC            densité de commentaire
     * @param CC            niveau de complexité de la méthode
     * @param BC            degré selon lequel la méthode est bien commentée
     */
    public MethodData(String methodName, String className, int LOC, int CLOC, double DC, int CC, double BC) {
        this.methodName = methodName;
        this.className = className;
        this.LOC = LOC;
        this.CLOC = CLOC;
        this.DC = DC;
        this.CC = CC;
        this.BC = BC;
    }

    public String getMethodName() {
        return methodName;
    }

    public String getClassName() {
        return className;
    }

    public int getLOC() {
        return LOC;
    }

    public int getCLOC() {
        return CLOC;
    }

    public double getDC() {
        return DC;
    }

    public int getCC() {
        return CC;
    }

    public double getBC() {
        return BC;
    }
}
