import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class CodeComplexityMeasurementTest {

    String classSample1 = "protected class Puppy extends Dog implements Motion {";
    String classSample2 = "// Adopt Me";
    String classSample3 = "\tvoid feed() {";
    String classSample4 = "\t\t/** Take me out of this misery */";
    String classSample5 = "\t}";
    String classSample6 = "}";

    CodeComplexityMeasurement javaFile1 = new CodeComplexityMeasurement();

    @Test
    public void testClass() {
        javaFile1.analyzeClassLine(classSample1);
        assertTrue(javaFile1.classIsOpen);
        assertEquals("Puppy", javaFile1.className);
        javaFile1.analyzeCommentsLine(classSample2);
        javaFile1.analyzeClassLine(classSample2);
        javaFile1.analyzeClassLine(classSample3);
        javaFile1.analyzeClassLine(classSample4);
        javaFile1.analyzeCommentsLine(classSample4);
        javaFile1.analyzeClassLine(classSample5);
        javaFile1.analyzeClassLine(classSample6);
        assertEquals(6, javaFile1.classLOC);
        assertEquals(2, javaFile1.classCLOC);
    }

    String methodSample1 = "/**";
    String methodSample2 = "Starts the car engine if keys are on and the car have enough fuel";
    String methodSample3 = "*/";
    String methodSample4 = "static void startEngine(boolean keys, int fuel) throws Exception {";
    String methodSample5 = "/* Requirements */";
    String methodSample6 = "\tif (!keys && fuel >= 20) {";
    String methodSample7 = "\t\tthis.Engine.setPower(true);";
    String methodSample8 = "\t}";
    String methodSample9 = "\telse if (!keys) {";
    String methodSample10 = "\t\t System.out.println(\"Keys were not in.\");";
    String methodSample11 = "\t}";
    String methodSample12 = "\telse if (fuel < 20) {";
    String methodSample13 = "\t\t System.out.println(\"Not enough fuel for the travel.\");";
    String methodSample14 = "\t}";
    String methodSample15 = "}";

    CodeComplexityMeasurement javaFile2 = new CodeComplexityMeasurement();

    @Test
    public void testMethod() {
        javaFile2.classIsOpen = true;
        javaFile2.className = "Name";
        javaFile2.analyzeCommentsLine(methodSample1);
        assertTrue(javaFile2.beforeMethode);
        javaFile2.analyzeCommentsLine(methodSample2);
        javaFile2.analyzeCommentsLine(methodSample3);
        assertEquals(3, javaFile2.methodLOC);
        assertFalse(javaFile2.beforeMethode);
        javaFile2.analyzeMethodsLine(methodSample4);
        assertTrue(javaFile2.methodIsOpen);
        javaFile2.analyzeMethodsLine(methodSample5);
        javaFile2.analyzeCommentsLine(methodSample5);
        javaFile2.analyzeMethodsLine(methodSample6);
        assertEquals(1, javaFile2.operationCount);
        javaFile2.analyzeMethodsLine(methodSample7);
        javaFile2.analyzeMethodsLine(methodSample8);
        javaFile2.analyzeCommentsLine(methodSample8);
        assertEquals(0, javaFile2.operationCount);
        javaFile2.analyzeMethodsLine(methodSample9);
        assertEquals(1, javaFile2.operationCount);
        javaFile2.analyzeMethodsLine(methodSample10);
        javaFile2.analyzeMethodsLine(methodSample11);
        assertEquals(0, javaFile2.operationCount);
        javaFile2.analyzeMethodsLine(methodSample12);
        javaFile2.analyzeMethodsLine(methodSample13);
        javaFile2.analyzeMethodsLine(methodSample14);
        assertEquals(14, javaFile2.methodLOC);
        assertEquals(4, javaFile2.methodCLOC);
        javaFile2.analyzeMethodsLine(methodSample15);
        assertFalse(javaFile2.methodIsOpen);
        assertTrue(javaFile2.classIsOpen);

    }

}